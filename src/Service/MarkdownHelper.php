<?php

declare(strict_types=1);

namespace App\Service;

use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Cache\CacheInterface;

final class MarkdownHelper
{
    private CacheInterface $cache;
    private bool $isDebug;
    private LoggerInterface $logger;
    private MarkdownParserInterface $markdownParser;
    private Security $security;

    public function __construct(
        MarkdownParserInterface $markdownParser,
        CacheInterface $cache,
        bool $isDebug,
        LoggerInterface $mdLogger,
        Security $security,
    ) {
        $this->cache = $cache;
        $this->isDebug = $isDebug;
        $this->logger = $mdLogger;
        $this->markdownParser = $markdownParser;
        $this->security = $security;
    }

    public function parse(string $source): string
    {
        if (false !== stripos($source, 'cat')) {
            $this->logger->info('Meow!');
        }

        if ($this->security->getUser()) {
            $this->logger->info(
                'Rendering markdown for {user}',
                ['user' => $this->security->getUser()->getUserIdentifier()]
            );
        }

        if ($this->isDebug) {
            return $this->markdownParser->transformMarkdown($source);
        }

        return $this->cache->get('markdown_'.md5($source), function () use ($source) {
            return $this->markdownParser->transformMarkdown($source);
        });
    }
}
