<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\Question;
use App\Entity\User;
use RuntimeException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

final class QuestionVoter extends Voter
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        return 'EDIT' === $attribute && $subject instanceof Question;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }
        if (!$subject instanceof Question) {
            throw new RuntimeException('Wrong type somehow passed');
        }

        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        return 'EDIT' === $attribute && $user === $subject->getOwner();
    }
}
