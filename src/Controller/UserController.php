<?php

declare(strict_types=1);

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class UserController extends BaseController
{
    #[Route('/api/me', name: 'app_user_api_me', methods: ['GET'])]
    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    public function apiMe(): JsonResponse
    {
        return $this->json(
            data: $this->getUser(),
            context: ['groups' => ['user:read']]
        );
    }
}
