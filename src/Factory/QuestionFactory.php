<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Question;
use App\Repository\QuestionRepository;
use DateTimeInterface;
use Exception;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Question>
 *
 * @method static         Question|Proxy createOne(array $attributes = [])
 * @method static         Question[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static         Question|Proxy find(object|array|mixed $criteria)
 * @method static         Question|Proxy findOrCreate(array $attributes)
 * @method static         Question|Proxy first(string $sortedField = 'id')
 * @method static         Question|Proxy last(string $sortedField = 'id')
 * @method static         Question|Proxy random(array $attributes = [])
 * @method static         Question|Proxy randomOrCreate(array $attributes = [])
 * @method static         Question[]|Proxy[] all()
 * @method static         Question[]|Proxy[] findBy(array $attributes)
 * @method static         Question[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static         Question[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static         QuestionRepository|RepositoryProxy repository()
 * @method Question|Proxy create(array|callable $attributes = [])
 */
final class QuestionFactory extends ModelFactory
{
    protected static function getClass(): string
    {
        return Question::class;
    }

    public function unpublished(): self
    {
        return $this->addState(['askedAt' => null]);
    }

    /**
     * @return array{name:string,question:string,askedAt:DateTimeInterface,votes:int}
     *
     * @throws Exception
     */
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->realText(50),
            'question' => self::faker()->paragraphs(
                self::faker()->numberBetween(1, 4),
                true
            ),
            'askedAt' => self::faker()->dateTimeBetween('-100 days', '-1 minute'),
            'votes' => random_int(-20, 50),
            'owner' => UserFactory::new(),
        ];
    }
}
